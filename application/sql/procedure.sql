DROP PROCEDURE IF EXISTS switchTable_cxy;

DELIMITER |

CREATE PROCEDURE switchTable_cxy() 
    -- pas de paramètres dans les parenthèses cxy_x double, cxy_y double, cxy_prof double
 

BEGIN
	DECLARE done BOOLEAN DEFAULT FALSE;
	DECLARE _x double;
    DECLARE _y double;
    DECLARE _prof double;
    
    DECLARE fuseau double default 7;
    DECLARE AMC double default 304800;
    DECLARE FEMC double default  0.9999;
    
    DECLARE a double default 6378137;
    DECLARE b double default 6356752.314;
    DECLARE m double default 111132.08937;
    
    /*construct*/
    DECLARE r double;
	DECLARE e double;
	DECLARE MCLamda double;
	DECLARE p double;
	 
    /*init*/
	DECLARE Byy double;
    DECLARE Bm_deg double;
    DECLARE Bm_Rad double;
    DECLARE phi_deg double;
    DECLARE phi_rad double;
    DECLARE b1 double;
	DECLARE xx double;
    
    /*xx*/
    DECLARE b1x double;
    DECLARE b1xp double;
    DECLARE b3 double;
    DECLARE b3x3 double;
    DECLARE b3x3p double;
    DECLARE longitude double;
    /*y*/
        
    DECLARE b2 double;
    DECLARE b2x2 double;
    DECLARE b2x2p double;
    DECLARE b4 double;
    DECLARE b4x4 double;
    DECLARE b4x4p double;
    DECLARE latitude double;
    
 
	DECLARE cur CURSOR FOR SELECT cxy_x, cxy_y, cxy_prof FROM coordonneexy_cxy;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done := TRUE;

	Open cur;
    
    testLoop: LOOP
    fetch cur into _x, _y, _prof;
    if done then 
    leave testLoop;
    end if;
    
    
    
	/* construct*/
    set r = (pow(a, 2) / b);
    set e = (pow(a, 2) - pow(b, 2) ) / pow(b, 2);
    set MCLamda = 51 + (3 * fuseau) - 1.5;
	set p = 180 / PI();
    
    
    /*init*/
   
    
	set Byy = _y / FEMC;
	set Bm_Deg = Byy / m;
	set Bm_Rad = radians(Bm_Deg);
	set phi_deg = (Bm_Deg) + (0.1459248684 * Sin(2 * Bm_Rad)) + (0.0002167967 * Sin(4 * Bm_Rad)) + (0.0000004411 * Sin(6 * Bm_Rad));
	set phi_rad = radians(phi_deg);
	set b1 = -(pow(((1 / pow(cos(phi_rad), 2)) + e), 0.5)) / (r);
	set xx = (_x - AMC) / FEMC;
    
    
    
    /*x*/
	set b1x = b1 * xx;
	set b1xp = b1x * p;
	set b3 = -(1 / 6) * pow(b1, 3) * (2 - pow(Cos(phi_rad), 2) + (e * pow(Cos(phi_rad), 2)));
	set b3x3 = b3 * pow(xx, 3);
	set b3x3p = b3x3 * p;
	set longitude = -(MCLamda + b1xp + b3x3p);
    

    
    
    /*Y*/
    set b2 = -(0.5 * pow(b1, 2) * Sin(phi_rad) * Cos(phi_rad) * (1 + (e * pow(Cos(phi_rad), 2))));
	set b2x2 = b2 * pow(xx, 2);
	set b2x2p = b2x2 * p;
	set b4 = -(1 / 12) * pow(b1, 2) * b2 * (3 + ((2 - (9 * e)) * pow(Cos(phi_rad), 2)) + (10 * e * pow(Cos(phi_rad), 4)) - (4 * pow(e, 2) * pow(Cos(phi_rad), 6)));
	set b4x4 = b4 * pow(xx, 4);
	set b4x4p = b4x4 * p;
	set latitude = phi_deg + b2x2p + b4x4p;
    
    
	/*insertion longitude latitude*/
    insert into coordonneeLL_cLL values (null,longitude, latitude, _prof);
    
    END loop testLoop;
    
    close cur;
END;
|
DELIMITER ;

Call switchTable_cxy();