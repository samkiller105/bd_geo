SET default_storage_engine=MYISAM;

drop table if exists coordonneeXY_cxy;
CREATE TABLE coordonneeXY_cxy (cxy_x double, cxy_y double, cxy_prof double);

drop table if exists coordonneeLL_cLL;

CREATE TABLE coordonneeLL_cLL 
(	cLL_id int auto_increment, 
	cLL_long double, 
	cLL_lat double, 
	cor_prof double,
	PRIMARY KEY (cLL_id)
);

drop table if exists coordonneeRando_cor;

CREATE TABLE coordonneeRando_cor 
(	cor_id int auto_increment, 
	cor_long double, 
	cor_lat double, 
	PRIMARY KEY (cor_id)
);

CREATE  INDEX x_LongLat
ON coordonneeLL_cLL (cLL_long, cLL_lat);

/*drop table if exists coordonnee_zone1_co1;
CREATE TABLE coordonnee_zone1_co1 (cor_id int, cor_long double, cor_lat double, cor_prof double);

drop table if exists coordonnee_zone2_co2;
CREATE TABLE coordonnee_zone2_co2 (cor_id int, cor_long double, cor_lat double, cor_prof double);

drop table if exists coordonnee_zone3_co3;
CREATE TABLE coordonnee_zone3_co3 (cor_id int, cor_long double, cor_lat double, cor_prof double);

drop table if exists coordonnee_zone4_co4;
CREATE TABLE coordonnee_zone4_co4 (cor_id int, cor_long double, cor_lat double, cor_prof double);

drop table if exists coordonnee_zone5_co5;
CREATE TABLE coordonnee_zone5_co5 (cor_id int, cor_long double, cor_lat double, cor_prof double);

drop table if exists coordonnee_zone6_co6;
CREATE TABLE coordonnee_zone6_co6 (cor_id int, cor_long double, cor_lat double, cor_prof double);

drop table if exists coordonnee_zone7_co7;
CREATE TABLE coordonnee_zone7_co7 (cor_id int, cor_long double, cor_lat double, cor_prof double);

drop table if exists coordonnee_zone8_co8;
CREATE TABLE coordonnee_zone8_co8 (cor_id int, cor_long double, cor_lat double, cor_prof double);

drop table if exists zone_zon;
CREATE TABLE zone_zon (zon_id varchar(32), zon_min_x double, zon_max_x double);*/
