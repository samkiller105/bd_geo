<style>/* Always set the map height explicitly to define the size of the div
 * element that contains the map. */
#map {
height: 100%;
    }
    /* Optional: Makes the sample page fill the window. */
    html, body {
    height: 100%;
    margin: 0;
    padding: 0;
}</style>
<!DOCTYPE html>

<!--[if IE 8]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en"> <!--<![endif]-->
<head>
    <title>TP2</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.ico">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,300italic,400italic' rel='stylesheet'
          type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <!-- Global CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css">
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/prism/prism.css">
    <!-- Theme CSS -->
    <link id="theme-style" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles.css">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body data-spy="scroll">

<!---//Facebook button code-->
<div id="fb-root"></div>
<script>(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<!-- ******HEADER****** -->
<header id="header" class="header">
    <div class="container">
        <h1 class="logo pull-left">
            <a class="scrollto" href="#promo">
                <span class="logo-title">Haut</span>
            </a>
        </h1><!--//logo-->
        <nav id="main-nav" class="main-nav navbar-right" role="navigation">
            <div class="navbar-header">
                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button><!--//nav-toggle-->
            </div><!--//navbar-header-->
            <div class="navbar-collapse collapse" id="navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active nav-item sr-only"><a class="scrollto" href="#promo">Home</a></li>
                    <li class="nav-item"><a class="scrollto" href="#about">About</a></li>
                    <li class="nav-item"><a class="scrollto" href="#features">Features</a></li>
                    <li class="nav-item"><a class="scrollto" href="#docs">Docs</a></li>
                    <li class="nav-item last"><a class="scrollto" href="#contact">Contact</a></li>
                </ul><!--//nav-->
            </div><!--//navabr-collapse-->
        </nav><!--//main-nav-->
    </div>
</header><!--//header-->

<!-- ******PROMO****** -->
<section id="promo" class="promo section offset-header">
    <div class="container text-center">
        <h2 class="title">Load<span class="highlight"> Points</span></h2>
        <div class="btns">
            <a class="btn btn-cta-secondary"  href="<?php echo base_url()?>index.php/Welcome/">Welcome</a>
            <a class="btn btn-cta-secondary"  href="<?php echo base_url()?>index.php/pointsInconnus/">Inconnus</a>
            <a class="btn btn-cta-secondary" href="<?php echo base_url() ?>index.php/Parcours/">Parcours</a>
        </div>
    </div><!--//container-->
    <div class="social-media">
        <div class="social-media-inner container text-center">
        </div>
    </div>
</section><!--//promo-->

<!-- ******ABOUT****** -->
<!-- ******DOCS****** -->
<section id="docs" class="docs section">
    <div class="container">
        <div class="docs-inner">
            <div class="block">

                <body>
                <h2>Choisissez le nombre de fichier à insérer en (BD)</h2>
                <div class="btns">
                    <a class="btn btn-cta-secondary"  href="<?php echo base_url()?>index.php/LoadFile/loadFile/1">1</a>
                    <a class="btn btn-cta-secondary"  href="<?php echo base_url()?>index.php/LoadFile/loadFile/2">2</a>
                    <a class="btn btn-cta-secondary"  href="<?php echo base_url()?>index.php/LoadFile/loadFile/3">3</a>
                    <a class="btn btn-cta-secondary"  href="<?php echo base_url()?>index.php/LoadFile/loadFile/4">4</a>
                    <a class="btn btn-cta-secondary"  href="<?php echo base_url()?>index.php/LoadFile/loadFile/5">5</a>
                    <a class="btn btn-cta-secondary"  href="<?php echo base_url()?>index.php/LoadFile/loadFile/6">6</a>
                    <a class="btn btn-cta-secondary"  href="<?php echo base_url()?>index.php/LoadFile/loadFile/7">7</a>
                    <a class="btn btn-cta-secondary"  href="<?php echo base_url()?>index.php/LoadFile/loadFile/8">8</a>
                </div>





</div><!--//code-block-->
</div><!--//block-->
</div><!--//docs-inner-->
</div><!--//container-->
</section><!--//features-->
<section id="about" class="about section">

</section><!--//about-->
<!-- ******CONTACT****** -->
<section id="contact" class="contact section has-pattern">
    <div class="container">

        <div class="clearfix"></div>
        <div class="info text-center">
            <h4 class="sub-title">Get Connected</h4>
            <ul class="social-icons list-inline">
                <li><a href="https://twitter.com/3rdwave_themes" target="_blank"><i class="fa fa-twitter"></i></a>
                </li>
                <li><a href="https://www.facebook.com/3rdwavethemes" target="_blank"><i class="fa fa-facebook"></i></a>
                </li>
                <li><a href="https://www.linkedin.com/in/xiaoying"><i class="fa fa-linkedin"></i></a></li>
                <li><a href="http://instagram.com/xyriley"><i class="fa fa-instagram"></i></a></li>
                <li><a href="https://dribbble.com/Xiaoying"><i class="fa fa-dribbble"></i></a></li>
                <li class="last"><a href="mailto: hello@3rdwavemedia.com"><i class="fa fa-envelope"></i></a></li>
            </ul>
        </div><!--//info-->
    </div><!--//contact-inner-->
    </div><!--//container-->
</section><!--//contact-->

<!-- ******FOOTER****** -->
<footer class="footer">
    <div class="container text-center">
        <small class="copyright">Designed with <i class="fa fa-heart"></i> by <a href="http://themes.3rdwavemedia.com"
                                                                                 target="_blank">Xiaoying Riley</a> for
            developers
        </small>
    </div><!--//container-->
</footer><!--//footer-->

<!-- Javascript -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/plugins/jquery-scrollTo/jquery.scrollTo.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/prism/prism.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/main.js"></script>
</body>
</html>

