<style>/* Always set the map height explicitly to define the size of the div
 * element that contains the map. */
    #map {
        height: 800px;
        width: 100%;
    }

    /* Optional: Makes the sample page fill the window. */
    html, body {
        height: 100%;
        margin: 0;
        padding: 0;
    }</style>
<!DOCTYPE html>

<!--[if IE 8]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en"> <!--<![endif]-->
<head>
    <title>TP2</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.ico">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,300italic,400italic' rel='stylesheet'
          type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <!-- Global CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css">
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/prism/prism.css">
    <!-- Theme CSS -->
    <link id="theme-style" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles.css">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body data-spy="scroll">

<!---//Facebook button code-->
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<!-- ******HEADER****** -->
<header id="header" class="header">
    <div class="container">
        <h1 class="logo pull-left">
            <a class="scrollto" href="#promo">
                <span class="logo-title">Haut</span>
            </a>
        </h1><!--//logo-->
        <nav id="main-nav" class="main-nav navbar-right" role="navigation">
            <div class="navbar-header">
                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button><!--//nav-toggle-->
            </div><!--//navbar-header-->
            <div class="navbar-collapse collapse" id="navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active nav-item sr-only"><a class="scrollto" href="#promo">Home</a></li>
                    <li class="nav-item"><a class="scrollto" href="#about">About</a></li>
                    <li class="nav-item"><a class="scrollto" href="#features">Features</a></li>
                    <li class="nav-item"><a class="scrollto" href="#docs">Docs</a></li>
                    <li class="nav-item last"><a class="scrollto" href="#contact">Contact</a></li>
                </ul><!--//nav-->
            </div><!--//navabr-collapse-->
        </nav><!--//main-nav-->
    </div>
</header><!--//header-->

<!-- ******PROMO****** -->
<section id="promo" class="promo section offset-header">
    <div class="container text-center">
        <h2 class="title">Affichage<span class="highlight"> Points</span></h2>
        <div class="btns">
            <a class="btn btn-cta-secondary" href="<?php echo base_url() ?>index.php/LoadFile/">LoadFile</a>
            <a class="btn btn-cta-secondary"  href="<?php echo base_url()?>index.php/pointsInconnus/">Inconnus</a>
            <a class="btn btn-cta-secondary" href="<?php echo base_url() ?>index.php/Parcours/">Parcours</a>
        </div>
    </div><!--//container-->
    <div class="social-media">
        <div class="social-media-inner container text-center">
        </div>
    </div>
</section><!--//promo-->

<!-- ******ABOUT****** -->
<!-- ******DOCS****** -->
<section id="docs" class="docs section">
    <div class="container">



                <body>
                <?php
                echo form_open('Parcours');
                echo form_label('Nombre de points', 'points');
                echo '<br>';
                echo form_input(array('name' => 'points'));
                echo form_submit('mysubmit', 'Go!');
                ?>

                <h3>My Google Maps</h3>
                <div id="map">
                    <!-- Replace the value of the key parameter with your own API key. -->


                    <script>
                        var map;
                        function initMap() {
                            map = new google.maps.Map(document.getElementById('map'), {
                                zoom: 10,
                                center: new google.maps.LatLng(<?php echo $tousPoints[0]['cor_lat']; ?>, <?php echo $tousPoints[0]['cor_long']; ?>),
                                mapTypeId: 'terrain'
                            });

                            // Create a <script> tag and set the USGS URL as the source.
                            var script = document.createElement('script');
                            // This example uses a local copy of the GeoJSON stored at
                            // http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/2.5_week.geojsonp
                            script.src = 'https://developers.google.com/maps/documentation/javascript/examples/json/earthquake_GeoJSONP.js';
                            document.getElementsByTagName('head')[0].appendChild(script);
                        }

                        // Loop through the results array and place a marker for each
                        // set of coordinates.
                        window.eqfeed_callback = function (results) {

                            <?php
                            $compteur = 0;
                            foreach ($tousPoints as $point) {

                                echo '
                                                           
                                var latLng = new google.maps.LatLng(' . $point['cor_lat'] . ',' . $point['cor_long'] . ');
                                
                                
                                var contentString'.strval($compteur).' = "<div><h1>Point</h1>Lattitude<p>' . $point['cor_lat'] . '</p>Longitude<p>' . $point['cor_long'] . '</p>Temps de calcul<p>'. strval( (microtime(true) - $_POST['temps'])) .'</p>'
                                . '<h1>Proche</h1>Lattitude<p>' . $point['cor_lat'] . '</p>Longitude<p>' . $point['cor_long'] . '</p>Temps de calcul<p>'. strval(0) .'</p></div>";   
                               
 
                                var marker'.strval($compteur).' = new google.maps.Marker({
                                    position: latLng,
                                    map: map
                                });
                                
                                marker'.strval($compteur).'.addListener(\'click\', function() {
                                  new google.maps.InfoWindow({
                                    content: contentString'.strval($compteur).'
                                  }).open(map, marker'.strval($compteur).');
                                  });
                                
                                ';
                                $compteur = $compteur+1;
                            }
                            ;
                            ?>
                        }
                    </script>
                </div>


                <script async defer
                        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDNDWdTxBo6Yi0pXYMbUJPMF7PMmCf6q2E&callback=initMap">
                </script>

                <?php
                if(isset($_POST['temps']))
                {
                    echo  strval( (microtime(true) - $_POST['temps']));
                }


                //   echo  "http://maps.google.com/maps?q=loc:" . array_values($tousPoints)[0]['cLL_lat'] . "," . array_values($tousPoints)[0]['cLL_long'];
                //   ECHO 'https://goo.gl/o8QuEq';

               /* $compteur = 0;
                foreach ($tousPoints as $point) {

                    if ($compteur == 0 || $compteur % 10 == 0) {
                        echo ' </br>                        ';
                    }

                    echo '<li>[' . $point['cLL_id'] . '] .  ' . $point['cLL_long'] . ',&nbsp' . $point['cLL_lat'] . '&nbsp(' . $point['cor_prof'] . ')</li>';
                    $compteur++;
                }


               */ ?>


            <!--//code-block-->


    </div><!--//container-->
</section><!--//features-->
<section id="about" class="about section">
</section><!--//about-->
<!-- ******CONTACT****** -->
<section id="contact" class="contact section has-pattern">
    <div class="container">

        <div class="clearfix"></div>
        <div class="info text-center">
            <h4 class="sub-title">Get Connected</h4>
            <ul class="social-icons list-inline">
                <li><a href="https://twitter.com/3rdwave_themes" target="_blank"><i class="fa fa-twitter"></i></a>
                </li>
                <li><a href="https://www.facebook.com/3rdwavethemes" target="_blank"><i class="fa fa-facebook"></i></a>
                </li>
                <li><a href="https://www.linkedin.com/in/xiaoying"><i class="fa fa-linkedin"></i></a></li>
                <li><a href="http://instagram.com/xyriley"><i class="fa fa-instagram"></i></a></li>
                <li><a href="https://dribbble.com/Xiaoying"><i class="fa fa-dribbble"></i></a></li>
                <li class="last"><a href="mailto: hello@3rdwavemedia.com"><i class="fa fa-envelope"></i></a></li>
            </ul>
        </div><!--//info-->
    </div><!--//contact-inner-->
    </div><!--//container-->
</section><!--//contact-->

<!-- ******FOOTER****** -->
<footer class="footer">
    <div class="container text-center">
        <small class="copyright">Designed with <i class="fa fa-heart"></i> by <a href="http://themes.3rdwavemedia.com"
                                                                                 target="_blank">Xiaoying Riley</a> for
            developers
        </small>
    </div><!--//container-->
</footer><!--//footer-->

<!-- Javascript -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/plugins/jquery-scrollTo/jquery.scrollTo.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/prism/prism.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/main.js"></script>
</body>
</html> 

