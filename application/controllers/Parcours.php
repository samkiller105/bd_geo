<?php

/**
 * Created by PhpStorm.
 * User: 1341526
 * Date: 2017-05-25
 * Time: 21:21
 */
class parcours extends CI_Controller
{



    public function __construct()
    {

        parent::__construct();
        ini_set('max_execution_time', 99999);
        $this->load->model("gestionBD");

        // 10 000 lignes (x) create statement () , procédure a la fin du load qui s'occupe des calculs de la table.


    }


    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {


        $data['titre'] = 'Affichage Donnes';


        if (!isset($_POST["nbrPoints"])) {
            $this->loadPoints(0);
        }

        $this->pointsAlea();
        //  $data['tousPoints'] = $this->gestionBD->getPoints($_POST["nbrPoints"]);


    }


    public function pointsAlea()
    {


        $nbrInsertion = $this->gestionBD->getNombreInsereXML();
        $nbrPointsTotal = $nbrInsertion[0]['cor_id'];
        $arrayPointsALoad = [];
        $arrayPoints = [];
        $nbrPoints = 0;
        $temps = null;

        if ($this->input->post('points') != null) {

            $_POST['temps'] = microtime(true);
            $nbrPoints = $this->input->post('points');
        }


        for ($i = 1; $i <= $nbrPoints; $i++) {


            foreach ($this->gestionBD->selectPointXML(strval(rand(0, $nbrPointsTotal))) as $point) {
                $arrayPoints[] = $point;
            }


        }

        $arrayPointsALoad['tousPoints'] = $arrayPoints;
        $this->load->view("parcours.php", $arrayPointsALoad);

        if ($temps!= null) {

            $temps = $temps - microtime(true);
            echo 'temps d\' exec ' . $_POST['temps']- microtime(true);
        }



    }


    public function loadPoints($index)
    {
        $_POST["nbrPoints"] = $index;
    }



}