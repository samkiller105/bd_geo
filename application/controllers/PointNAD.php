<?php
//Mise en oeuvre par Robert Aubé
//30 avril 2017
//version 1.0

Class PointNAD extends CI_controller{

    public $noPoint;
    public $X;
    public $Y;
    public $latitude;
    public $longitude;
    public $googleLink;

// NAD83
    const fuseau = 7;
//ABSCISSE DU MERIDIEN CENTRAL (Xo)
    const AMC = 304800;
//FACTEUR-ECHELLE AU MERIDIEN CENTRAL (Ko)
    const FEMC = 0.9999;

//MERIDIEN CENTRAL (LAMDAo)
    private static $MCLamda;

//ELLIPSOIDE DE REFERENCE
    const a = 6378137;
    const b = 6356752.314;
    const m = 111132.08937;

    private static $p;
    private static $r;
    private static $e;
    private $By;
    private $Bm_Deg;
    private $Bm_Rad;
    private $phi_deg;
    private $phi_rad;
    private $b1;
    private $xx;

    public function __construct($noPoint, $X, $Y) {

        parent:: __construct();

        self::$r = (pow(self::a, 2) / self::b);
        self::$e = (pow(self::a, 2) - pow(self::b, 2) ) / pow(self::b, 2);
        self::$MCLamda = 51 + (3 * self::fuseau) - 1.5;
        self::$p = 180 / 3.1415926535898;

        $this->noPoint = $noPoint;
        $this->set($X, $Y);
    }

    public function set($X, $Y) {
        $this->X = $X;
        $this->Y = $Y;
        $this->initNAD83($X, $Y);
    }

    public static function test() {
        //cegep Limoilou
        //46°49'49.3"N 71°13'37.8"W
        //46.830371, -71.227174
        $pCegepLimoilou = new PointNAD(1, 249325, 5187900);

        echo "<br>latitude [46.83029107]: " . $pCegepLimoilou->latitude;
        echo "<br>longitude [-71.22717161]: " . $pCegepLimoilou->longitude;
        echo "<br>googleLink : " . $pCegepLimoilou->googleLink;
    }

    private function initNAD83($X, $Y) {
        $this->By = $Y / self::FEMC;
        $this->Bm_Deg = $this->By / self::m;
        $this->Bm_Rad = deg2rad($this->Bm_Deg);
        $this->phi_deg = ($this->Bm_Deg) + (0.1459248684 * Sin(2 * $this->Bm_Rad)) + (0.0002167967 * Sin(4 * $this->Bm_Rad)) + (0.0000004411 * Sin(6 * $this->Bm_Rad));
        $this->phi_rad = deg2rad($this->phi_deg);
        $this->b1 = -(pow(((1 / pow(cos($this->phi_rad), 2)) + self::$e), 0.5)) / (self::$r);
        $this->xx = ($this->X - self::AMC) / self::FEMC;

        $this->setLonNAD83();
        $this->setLatNAD83();
    }

	//TRANSFORMATIONS DE COORDONNEES RECTANGULAIRES A GEODESIQUES (longitude E-W)
    private function setLonNAD83() {
        //variables locals
        //$b1x
        //$b1xp 
        //$b3 
        //$b3x3 
        //$b3x3p 
        //LAMDA
        $b1x = $this->b1 * $this->xx;
        $b1xp = $b1x * self::$p;
        $b3 = -(1 / 6) * pow($this->b1, 3) * (2 - pow(Cos($this->phi_rad), 2) + (self::$e * pow(Cos($this->phi_rad), 2)));
        $b3x3 = $b3 * pow($this->xx, 3);
        $b3x3p = $b3x3 * self::$p;
        $this->longitude = -(self::$MCLamda + $b1xp + $b3x3p);
    }

    //TRANSFORMATIONS DE COORDONNEES RECTANGULAIRES A GEODESIQUES (latitude N-S)
    private function setLatNAD83() {
        // $b2
        // $b2x2
        // $b4
        // $b4x4
        // $b2x2p
        // $b4x4p
        // PHI
        $b2 = -(0.5 * pow($this->b1, 2) * Sin($this->phi_rad) * Cos($this->phi_rad) * (1 + (self::$e * pow(Cos($this->phi_rad), 2))));
        $b2x2 = $b2 * pow($this->xx, 2);
        $b2x2p = $b2x2 * self::$p;
        $b4 = -(1 / 12) * pow($this->b1, 2) * $b2 * (3 + ((2 - (9 * self::$e)) * pow(Cos($this->phi_rad), 2)) + (10 * self::$e * pow(Cos($this->phi_rad), 4)) - (4 * pow(self::$e, 2) * pow(Cos($this->phi_rad), 6)));
        $b4x4 = $b4 * pow($this->xx, 4);
        $b4x4p = $b4x4 * self::$p;
        $this->latitude = $this->phi_deg + $b2x2p + $b4x4p;
    }

	//détermine la distance entre 2 points en mètres
    public Function getDistanceM($latitude, $longitude) {
        //ref: http://www.movable-type.co.uk/scripts/latlong.html
        $R = 6371000; //metres
        $latitude1 = deg2rad($this->latitude);
        $latitude2 = deg2rad($latitude);
        $delta_latitude = deg2rad($latitude - $this->latitude);
        $delta_longitude = deg2rad($longitude - $this->longitude);

        $a = Sin($delta_latitude / 2) * Sin($delta_latitude / 2) +
                Cos($latitude1) * Cos($latitude2) *
                Sin($delta_longitude / 2) * Sin($delta_longitude / 2);

        $c = 2 * Atan2(Sqrt($a), Sqrt(1 - $a));

        $d = $R * $c;

        return $d;
    }

}
