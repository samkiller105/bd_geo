<?php

/**
 * Created by PhpStorm.
 * User: W40000IT7000
 * Date: 2017-05-24
 * Time: 20:23
 */
class LoadFile extends CI_Controller
{

    public function __construct()
    {
        parent:: __construct();

        $this->load->model("gestionBD");

    }

    public function index()
    {
        $this->load->view('loadFile.php');
    }

    public function loadFile($nbrFichier)
    {

        if (!isset($_POST["tempsEffectue"])) {
            $_POST["tempsEffectue"] = microtime(true);
        }

        if (!isset($_POST["nbrFichierLoad"])) {
            $_POST["nbrFichierLoad"] = $nbrFichier;
        }

        $_POST["nbrFichierLoad"];

        $this->gestionBD->dropCreateTables();

        if (!isset($_POST["tempsEffectue"])) {
            $_POST["tempsEffectue"] = microtime(true);
        }

        $this->load->model("File_loader");

        $this->gestionBD->effectuerCalculs();


        $this->afficherInformationsChargement();

    }

    public function afficherInformationsChargement()
    {


        $tempsEffectue = 'NA';

        $data['tempsEffectue'] = strval(microtime(true) - $_POST["tempsEffectue"]);
        $nbrInsertion = $this->gestionBD->getNombreInsere();
        $data['nbrInsere'] = $nbrInsertion[0]['cll_id'];
        $data['fichiersInseres'] = $this->File_loader->getArrayInsertionsFichier();

        $this->load->view('informationLoad', $data);
    }


}