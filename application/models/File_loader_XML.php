<?php

/**
 * Created by PhpStorm.
 * User: 1341526
 * Date: 2017-05-25
 * Time: 21:22
 */
class File_loader_XML extends CI_Model
{


    public function __construct()
    {
        parent:: __construct();
        $this->openFile();
        $this->load->model('gestionBD');


    }


    public function openFile()
    {


        $filePath = "files/activity_890629920.xml";
        $xml = simplexml_load_file($filePath) or die("Error: Cannot create object");

        $arrayInsert = array();

        foreach ($xml->trk->trkseg->children() as $trkseg) {

            array_push($arrayInsert, '(null,' . $trkseg['lon'] . ',' . $trkseg['lat'] . ')');

        }

        $this->gestionBD->createStatementXML($arrayInsert);


        $filePath = "files/activity_1219107562.xml";
        $xml = simplexml_load_file($filePath) or die("Error: Cannot create object");

        $arrayInsert = array();

        foreach ($xml->trk->trkseg->children() as $trkseg) {

            array_push($arrayInsert, '(null,' . $trkseg['lon'] . ',' . $trkseg['lat'] . ')');

        }

        $this->gestionBD->createStatementXML($arrayInsert);


    }


}