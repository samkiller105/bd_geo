<?php

/**
 * Created by PhpStorm.
 * User: 1341526
 * Date: 2017-04-26
 * Time: 10:01
 */
class gestionBD extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }




    public function getNombreInsere()
    {
        return $this->db->query('SELECT cll_id FROM coordonneeLL_cLL WHERE cll_id =(SELECT max(cll_id) FROM coordonneeLL_cLL)')->result_array();
    }

    public function getNombreInsereXML()
    {
        return $this->db->query('SELECT cor_id FROM coordonneeRando_cor WHERE cor_id =(SELECT max(cor_id) FROM coordonneeRando_cor)')->result_array();
    }

    public function dropCreateTables()
    {
        $this->db->query('truncate table coordonneeLL_cLL');
        $this->db->query('truncate table coordonneexy_cxy');
    }

    public function selectPoint($id)
    {


        return $this->db->query('select * from coordonneell_cll where cll_id =' . $id)->result_array();
    }

    public function selectPointXML($id)
    {


        return $this->db->query('select * from coordonneeRando_cor where cor_id =' . $id)->result_array();
    }

    public function effectuerCalculs()
    {
        $this->db->query('Call switchTable_cxy()');
    }


    public function getPoints($indexDepart)
    {


        if ($indexDepart <= 0) {
            $indexDepart = 0;
        } else if ($indexDepart >= '1740000') {
            $indexDepart = 1740000 - 1000;
        }

        $query = $this->db->query('SELECT * FROM coordonneell_cll LIMIT ' . $indexDepart . ', 10');

        return $query->result_array();
    }


    public function createStatement($arrayInsertion)
    {

        $stringInsert = 'insert into coordonneexy_cxy values';

        foreach ($arrayInsertion as $insert) {
            $stringInsert = $stringInsert . $insert . ',';

        }

        $this->db->query(rtrim($stringInsert, ", "));


    }

    public function createStatementXML($arrayInsertion)
    {

        $stringInsert = 'insert into coordonneeRando_cor values';

        foreach ($arrayInsertion as $insert) {
            $stringInsert = $stringInsert . $insert . ',';

        }

        $this->db->query(rtrim($stringInsert, ", "));


    }

    public function getPointsOffsets($point)
    {

        $_POST['tempsProche'] = microtime(true);
        $arrayPointEtProche = [];
        $arrayPointEtProche[] = $point;

        $pointsProches = $this->gestionBD->getPointsPlusProches($point['cLL_lat'], $point['cLL_long']);

        $pointLePlusProche = $pointsProches[0];
        $distanceLaPlusCourte = 10000;


        foreach ($pointsProches as $pointActuel) {


            //calcul de distance
            $distance = $this->calculerDistanceDeuxPoints($pointLePlusProche, $pointActuel);
            //calcul
            if ($distance <= $distanceLaPlusCourte) {

                $distanceLaPlusCourte = $distance;
                $pointLePlusProche = $pointActuel;

            }
        }

        $pointLePlusProche['distance'] = $this->calculerDistanceDeuxPoints($pointLePlusProche, $point);

        $arrayPointEtProche[] = $pointLePlusProche;
        return $arrayPointEtProche;

    }


    public function calculerDistanceDeuxPoints($point1, $point2)
    {

        $distance = sqrt(pow($point1['cLL_long'] - $point2['cLL_long'], 2) + pow($point1['cLL_lat'] - $point2['cLL_lat'], 2));
        return $distance;

    }


    public function getPointsPlusProches($pointLat, $pointLong)
    {
        $tousLesPoints = [];

        $pointsLong = $this->db->query('SELECT cLL_id ,cLL_long,cLL_lat, ABS( cLL_long + ' . $pointLong . ' ) AS distance FROM (
        (
        SELECT cLL_id,cLL_long,cLL_lat
		FROM coordonneell_cll
		WHERE cLL_long >=' . $pointLong . '
		ORDER BY cLL_long
		LIMIT 5
	    ) UNION ALL (
        SELECT cLL_id,cLL_long,cLL_lat
		FROM coordonneell_cll
		WHERE cLL_long < ' . $pointLong . '
		ORDER BY cLL_long DESC
		LIMIT 5
            )
        ) AS n 
        ORDER BY distance
        LIMIT 5')->result_array();


        $pointLat = $this->db->query('SELECT cLL_id ,cLL_lat,cLL_long, ABS( cLL_lat - ' . $pointLat . ' ) AS distance FROM (
        (
        SELECT cLL_id,cLL_lat,cLL_long
		FROM coordonneell_cll
		WHERE cLL_lat >=' . $pointLat . '
		ORDER BY cLL_lat
		LIMIT 5
	) UNION ALL (
        SELECT cLL_id,cLL_lat,cLL_long
		FROM coordonneell_cll
		WHERE cLL_lat < ' . $pointLat . '
		ORDER BY cLL_lat DESC
		LIMIT 5
            )
        ) AS n 
        ORDER BY distance
        LIMIT 5')->result_array();

        foreach ($pointsLong as $point) {
            $tousLesPoints[] = $point;
        }

        foreach ($pointLat as $point) {
            $tousLesPoints[] = $point;
        }


        return $tousLesPoints;

    }
    public function selectMaxLat($betweenDebut, $betweenFin)
    {
        
       return $this->db->query('select max(cLL_lat)  from coordonneell_cll')->result_array();
    }

    public function selectMinLat($betweenDebut, $betweenFin)
    {

        return $this->db->query('select min(cLL_lat)  from coordonneell_cll where cLL_id between ' . strval($betweenDebut) . ' AND ' . strval($betweenFin) .'')->result_array();
    }

    public function selectMaxLong($betweenDebut, $betweenFin)
    {

        return $this->db->query('select max(cLL_long)  from coordonneell_cll where cLL_id between ' . $betweenDebut . ' AND ' . $betweenFin.'')->result_array();
    }

    public function selectMinLong($betweenDebut, $betweenFin)
    {
       
        return $this->db->query('select *  from coordonneell_cll where  cLL_long = (SELECT max(cll_long) FROM coordonneeLL_cLL where coordonneell_cll.cLL_id between '.$betweenDebut .' AND '.$betweenFin .')')->result_array();
    }

}

