<?php

/**
 * Created by PhpStorm.
 * User: 1341526
 * Date: 2017-05-18
 * Time: 11:05
 */
class File_loader extends CI_Model
{
    private $nbrLignesInsertion = 10000;
    private $arrayInfosInsertionsFichier = [];
    private $arrayMaximumZone = [];
    private $compteurGlobal = 1;

    public function __construct()
    {
        parent:: __construct();
        ini_set('max_execution_time', 99999);
        $this->load->model('gestionBD');

        $nbrFichier = 0;
        if (isset($_POST["nbrFichierLoad"])) {
            $nbrFichier = $_POST["nbrFichierLoad"];
        }


        for ($i = 1; $i <= $nbrFichier; $i++) {

            $this->loadFichier($i);
        }

    }

    public function loadFichier($numFichier)
    {
        $zone = $this->getFileWithIndex($numFichier);
        $contenuFichier = file(base_url() . '/files/' . 'Zone' . $zone . '_MTM_7.txt');

        $this->arrayInfosInsertionsFichier['fichier' . $numFichier] = $this->insertionBDFichier($numFichier, $contenuFichier);

    }

    public function getArrayInsertionsFichier()
    {
        return $this->arrayInfosInsertionsFichier;
    }

    public function insertionBDFichier($numfichier, $contenuFichier)
    {
        $this->arrayMaximumZone[$numfichier]['min'] = $this->compteurGlobal;

        $arrayInformationsInsertion = [];
        $temp = microtime(true);


        $arrayInsert = array();
        $compteur = 0;
        foreach ($contenuFichier as $line) {

            if ($compteur == $this->nbrLignesInsertion) {
                $this->gestionBD->createStatement($arrayInsert);
                $arrayInsert = array();
                $compteur = 0;

            }
            $values = explode(" ", $line);

            array_push($arrayInsert, '(' . $values[0] . ',' . $values[1] . ',' . $values[2] . ')');
            $compteur += 1;
            $this->compteurGlobal += 1;
        }

        $this->gestionBD->createStatement($arrayInsert);


        $this->arrayMaximumZone[$numfichier]['max'] = $this->compteurGlobal;


       /* $arrayInformationsInsertion['MinLat'] = $this->gestionBD->selectMinLat(intval ($this->arrayMaximumZone[$numfichier]['min']), intval ($this->arrayMaximumZone[$numfichier]['max']))[0]['min(cLL_lat)'];
        $arrayInformationsInsertion['MaxLat'] = $this->gestionBD->selectMaxLat(intval ($this->arrayMaximumZone[$numfichier]['min']), intval ($this->arrayMaximumZone[$numfichier]['max']))[0]['max(cLL_lat)'];
        $arrayInformationsInsertion['MinLong'] = $this->gestionBD->selectMinLong(intval ($this->arrayMaximumZone[$numfichier]['min']), intval ($this->arrayMaximumZone[$numfichier]['max']))[0]['min(cLL_long)'];
        $arrayInformationsInsertion['MaxLong'] = $this->gestionBD->selectMaxLong(intval ($this->arrayMaximumZone[$numfichier]['min']), intval ($this->arrayMaximumZone[$numfichier]['max']))[0];*/
        $arrayInformationsInsertion['premierIndex'] = $this->arrayMaximumZone[$numfichier]['min'];
        $arrayInformationsInsertion['dernierIndex'] = $this->arrayMaximumZone[$numfichier]['max'];
        $arrayInformationsInsertion['tempChargement'] = (microtime(true) - $temp);
        return $arrayInformationsInsertion;
    }


    public function getContenuFichier()
    {
        return $this->contenuFichier;
    }


    public function getZoneWithIndex($index)
    {

        $lettre = "";

        switch ($index) {
            case  $index > $this->arrayMaximumZone[1]['min']:
                $lettre = 'A';
                break;
            case $index >= $this->arrayMaximumZone[2]['min']:
                $lettre = 'B';
                break;
            case $index >= $this->arrayMaximumZone[3]['min'] :
                $lettre = 'C';
                break;
            case $index >= $this->arrayMaximumZone[4]['min']:
                $lettre = 'D';
                break;
            case $index >= $this->arrayMaximumZone[5]['min']:
                $lettre = 'E';
                break;
            case $index >= $this->arrayMaximumZone[6]['min']:
                $lettre = 'F';
                break;
            case $index >= $this->arrayMaximumZone[7]['min']:
                $lettre = 'G';
                break;
            case $index >= $this->arrayMaximumZone[8]['min'] :
                $lettre = 'H';
                break;
        }


        return $lettre;
    }


    public function getFileWithIndex($index)
    {
        $lettre = '';

        switch ($index) {
            case 1:
                $lettre = 'A';
                break;
            case 2:
                $lettre = 'B';
                break;
            case 3 :
                $lettre = 'C';
                break;
            case 4:
                $lettre = 'D';
                break;
            case 5:
                $lettre = 'E';
                break;
            case 6:
                $lettre = 'F';
                break;
            case 7:
                $lettre = 'G';
                break;
            case 8 :
                $lettre = 'H';
                break;
        }


        return $lettre;

    }
}